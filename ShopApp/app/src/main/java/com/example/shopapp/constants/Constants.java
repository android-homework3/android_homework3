package com.example.shopapp.constants;

public class Constants {

    public static String BASE_URL = "https://my-json-server.typicode.com";
    public static String TITLE = "title";
    public static String ID = "id";
    public static String CATEGORYID = "categoryId";
    public static String PRICE = "pret";
    public static String DESCRIPTION = "descriere";
    public static String URL = "url";

}
