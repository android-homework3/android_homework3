package com.example.shopapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.example.shopapp.R;
import com.example.shopapp.interfaces.OnItemClick;
import com.example.shopapp.models.Products;
import com.example.shopapp.models.ProductsDatabase;
import com.example.shopapp.singleton.VolleyConfigSingleton;

import java.util.ArrayList;

public class ProductsDatabaseAdapter extends RecyclerView.Adapter<ProductsDatabaseAdapter.ProductsViewHolder> {

    private ArrayList<ProductsDatabase> products;

    private OnItemClick onItemClick;

    public ProductsDatabaseAdapter(ArrayList<ProductsDatabase> products) {
        this.products = products;

    }

    @NonNull
    @Override
    public ProductsDatabaseAdapter.ProductsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());


        View view = inflater.inflate(R.layout.product_item_cell, parent, false);
        ProductsViewHolder productsViewHolder = new ProductsViewHolder(view);

        return productsViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsDatabaseAdapter.ProductsViewHolder holder, int position) {

        ProductsDatabase product = (ProductsDatabase) products.get(position);
        ((ProductsDatabaseAdapter.ProductsViewHolder) holder).bind(product);
    }

    @Override
    public int getItemCount() {
        return this.products.size();
    }

    class ProductsViewHolder extends RecyclerView.ViewHolder {

        private TextView description;
        private TextView price;
        private ImageView imageView;
        View view;

        public ProductsViewHolder(@NonNull View itemView) {
            super(itemView);

            description = itemView.findViewById(R.id.txt_description);
            price = itemView.findViewById(R.id.txt_price);
            imageView = itemView.findViewById(R.id.img_product);
            this.view = itemView;

        }


        void bind(ProductsDatabase productsObj) {
            description.setText(productsObj.getDescription());
            String priceValue = "" + productsObj.getPrice();
            price.setText(priceValue);
            String imageUrl = productsObj.getUrl().toString();
            ImageLoader imageLoader = VolleyConfigSingleton.getInstance(imageView.getContext().getApplicationContext()).getImageLoader();

            imageLoader.get(imageUrl, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    imageView.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClick != null) {

                        notifyItemChanged(getAdapterPosition());
                    }
                }
            });

        }

    }
}
