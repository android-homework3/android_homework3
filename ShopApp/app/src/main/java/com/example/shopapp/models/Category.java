package com.example.shopapp.models;

import java.util.ArrayList;

public class Category {
    private String title;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Category(String title, String id) {
        this.title = title;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Category(String title) {
        this.title = title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
