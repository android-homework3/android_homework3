package com.example.shopapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shopapp.R;
import com.example.shopapp.adapters.ProductsAdapter;
import com.example.shopapp.adapters.ProductsDatabaseAdapter;
import com.example.shopapp.interfaces.ActivitiesFragmentsCommunication;
import com.example.shopapp.models.Products;
import com.example.shopapp.models.ProductsDatabase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class HomePage extends Fragment {

    public static final String TAG_HOME_PAGE = "TAG_HOME_PAGE";

    private ActivitiesFragmentsCommunication fragmentCommunication;

    FirebaseDatabase database = FirebaseDatabase.getInstance("https://shopapp-android-2021-default-rtdb.europe-west1.firebasedatabase.app/");
    DatabaseReference myRef = database.getReference();

    ArrayList<ProductsDatabase> books = new ArrayList<ProductsDatabase>();

    RecyclerView bookList;


    ProductsDatabaseAdapter bookAdapter;

    public static HomePage newInstance() {

        Bundle args = new Bundle();

        HomePage fragment = new HomePage();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        //writeNewProducts("299","Colier strălucitor de tip infinity","https://cdn.shopify.com/s/files/1/0521/8009/1068/products/PNGTRPNT_398821C01_RGB_c4b9ade2-9d05-49b2-bf40-5e843f33cfc0_1024x1024@2x.png?v=1609876270");
        View view = inflater.inflate(R.layout.fragment_home_page, container, false);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        bookList = (RecyclerView) view.findViewById(R.id.popular_products_list);
        bookList.setLayoutManager(linearLayoutManager);
       /* books.add(new ProductsDatabase("339","Brățară tip lanț cu coroană Pandora Moments","https://cdn.shopify.com/s/files/1/0521/8009/1068/products/PNGTRPNT_598286CZ_RGB_2b285bcb-00d7-4d81-bb34-7b1341f25493_1024x1024@2x.png?v=1609868835"));
        books.add(new ProductsDatabase("259","Talisman bicicletă din argint 925, cu zirconiu cubic","https://cdn.shopify.com/s/files/1/0521/8009/1068/products/PNGTRPNT_797858CZ_V2_RGB_1024x1024@2x.png?v=1614787115"));
        books.add(new ProductsDatabase("299","Inel dorință de prințesă","https://cdn.shopify.com/s/files/1/0521/8009/1068/products/197736CZ_RGB_1024x1024@2x.png?v=1609877111"));
        books.add(new ProductsDatabase("259","Cercei contur de fluturi","https://cdn.shopify.com/s/files/1/0521/8009/1068/products/297912CZ_RGB_441a401c-e59c-4a0a-9409-ded993107e4b_1024x1024@2x.png?v=1609869050"));
        books.add(new ProductsDatabase("299","Colier strălucitor de tip infinity","https://cdn.shopify.com/s/files/1/0521/8009/1068/products/PNGTRPNT_398821C01_RGB_c4b9ade2-9d05-49b2-bf40-5e843f33cfc0_1024x1024@2x.png?v=1609876270"));
        */
        getDataFromDatabase();

        bookAdapter = new ProductsDatabaseAdapter(books);
        bookList.setAdapter(bookAdapter);
        return view;

    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication) {
            fragmentCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    public void getDataFromDatabase() {

        books.clear();
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    ProductsDatabase data = ds.getValue(ProductsDatabase.class);

                    ProductsDatabase book = new ProductsDatabase(data.getPrice(), data.getDescription(), data.getUrl());
                    books.add(book);

                    bookAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        myRef.addListenerForSingleValueEvent(eventListener);
    }


//    public void writeNewProducts(String price,String description,String url) {
//        ProductsDatabase product = new ProductsDatabase(price, description,url);
//        myRef.child("products").push().setValue(product);
//    }

    public void writeNewProducts(String price, String description, String url) {
        ProductsDatabase product = new ProductsDatabase(price, description, url);
        myRef.push().setValue(product);
    }


}
