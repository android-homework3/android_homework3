package com.example.shopapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.shopapp.interfaces.ActivitiesFragmentsCommunication;
import com.example.shopapp.models.Category;

public class AboutActivity extends AppCompatActivity implements ActivitiesFragmentsCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
//        Button button=(Button) findViewById(R.id.btn_about);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                goToHomeActivity();
//            }
//        });
        TextView textView = (TextView) findViewById(R.id.txt_about);
        textView.setMovementMethod(new ScrollingMovementMethod());
    }

    //    public void goToHomeActivity()
//    {
//        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
//        startActivity(intent);
//        finish();
//    }
    @Nullable
    @Override
    public View onCreateView(@NonNull String name, @NonNull Context context, @NonNull AttributeSet attrs) {
        return super.onCreateView(name, context, attrs);

    }

    @Override
    public void onReplaceFragment(String TAG) {

    }

    @Override
    public void replaceProductsPageFragment(Category category) {

    }

    @Override
    public void addHomePageFragment() {

    }

}