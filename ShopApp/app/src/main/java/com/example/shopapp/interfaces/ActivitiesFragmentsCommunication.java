package com.example.shopapp.interfaces;

import com.example.shopapp.models.Category;

public interface ActivitiesFragmentsCommunication {

    void onReplaceFragment(String TAG);

    void replaceProductsPageFragment(Category category);

    void addHomePageFragment();

}
