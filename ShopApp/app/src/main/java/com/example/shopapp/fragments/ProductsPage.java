package com.example.shopapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.shopapp.R;
import com.example.shopapp.adapters.ProductsAdapter;
import com.example.shopapp.interfaces.ActivitiesFragmentsCommunication;
import com.example.shopapp.models.Category;
import com.example.shopapp.models.Products;
import com.example.shopapp.singleton.VolleyConfigSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.shopapp.constants.Constants.BASE_URL;
import static com.example.shopapp.constants.Constants.CATEGORYID;
import static com.example.shopapp.constants.Constants.DESCRIPTION;
import static com.example.shopapp.constants.Constants.ID;
import static com.example.shopapp.constants.Constants.PRICE;
import static com.example.shopapp.constants.Constants.URL;

public class ProductsPage extends Fragment {
    public static final String TAG_PRODUCTS_PAGE = "TAG_PRODUCTS_PAGE";
    public static final String ARG_ID = "ARG_ID";
    private ActivitiesFragmentsCommunication fragmentCommunication;

    private ArrayList<Products> products = new ArrayList<Products>();

    ProductsAdapter productsAdapter = new ProductsAdapter(products);




    public static ProductsPage newInstance(Category category) {

        Bundle args = new Bundle();

        args.putString(ARG_ID,category.getId());
        ProductsPage fragment = new ProductsPage();
        fragment.setArguments(args);
        return fragment;
    }

    private String getCategoryIdFromArguments(){
        return getArguments().getString(ARG_ID);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_products_page, container, false);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        RecyclerView albumList = (RecyclerView) view.findViewById(R.id.products_list);
        albumList.setLayoutManager(linearLayoutManager);
        albumList.setAdapter(productsAdapter);
        return view;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ActivitiesFragmentsCommunication) {
            fragmentCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getProducts();

    }

    public void getProducts() {
        VolleyConfigSingleton volleySingleton = VolleyConfigSingleton.getInstance(getContext());
        RequestQueue queue = volleySingleton.getRequestQueue();
        String url = BASE_URL + "/Radina2000/json_android/categories/";
        url = url + getCategoryIdFromArguments() + "/products";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        handleProductsResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getContext(), "Error!", Toast.LENGTH_SHORT).show();
                    }
                });
        queue.add(stringRequest);
    }


    public void handleProductsResponse(String response) {
        try {

            products.clear();
            JSONArray productsJsonArray = new JSONArray(response);

            for (int index = 0; index < productsJsonArray.length(); index++) {

                JSONObject productsJsonObject = productsJsonArray.getJSONObject(index);

                if (productsJsonObject != null) {
                    int categoryId = productsJsonObject.getInt(CATEGORYID);
                    int id = productsJsonObject.getInt(ID);
                    String description = productsJsonObject.getString(DESCRIPTION);
                    int price = productsJsonObject.getInt(PRICE);
                    String url = productsJsonObject.getString(URL);

                    Products product = new Products(id, price, description, url, categoryId);


                    products.add(product);
                }
            }

            productsAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
