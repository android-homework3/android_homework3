package com.example.shopapp.models;

public class ProductsDatabase {

    private String price;
    private String description;
    private String url;


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    public ProductsDatabase(String price, String description, String url) {
        this.price = price;
        this.description = description;
        this.url = url;
    }

    public ProductsDatabase() {

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
