package com.example.shopapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.shopapp.fragments.Category;
import com.example.shopapp.fragments.HomePage;
import com.example.shopapp.fragments.ProductsPage;
import com.example.shopapp.interfaces.ActivitiesFragmentsCommunication;
import com.google.firebase.auth.FirebaseAuth;

import static com.example.shopapp.fragments.Category.TAG_CATEGORY;
import static com.example.shopapp.fragments.HomePage.TAG_HOME_PAGE;
import static com.example.shopapp.fragments.ProductsPage.TAG_PRODUCTS_PAGE;

public class HomeActivity extends AppCompatActivity implements ActivitiesFragmentsCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (savedInstanceState == null) {
            onAddHomeFragment();
        }
    }

    private void onAddHomeFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.home_frame_layout, Category.newInstance(), TAG_CATEGORY);

        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.right_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                FirebaseAuth.getInstance().signOut();
                goToWelcome();
                return true;
            case R.id.about:
                Toast.makeText(this, "About Item ", Toast.LENGTH_SHORT).show();
                goToAboutActivity();
                return true;
            case R.id.page_home:
                onReplaceFragment(TAG_CATEGORY);
                Toast.makeText(this, "Home Page ", Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void goToWelcome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void goToAboutActivity() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }


    @Override
    public void onReplaceFragment(String TAG) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment;

        switch (TAG) {
            case TAG_CATEGORY: {
                fragment = Category.newInstance();
                break;
            }

            default:
                return;
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.home_frame_layout, fragment, TAG);

        fragmentTransaction.commit();
    }

    @Override
    public void replaceProductsPageFragment(com.example.shopapp.models.Category category) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        FragmentTransaction addTransaction = transaction.replace(R.id.frame_layout_home, ProductsPage.newInstance(category), TAG_PRODUCTS_PAGE);

        // addTransaction.addToBackStack(TAG_PRODUCTS_PAGE);

        addTransaction.commit();

    }

    @Override
    public void addHomePageFragment() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        FragmentTransaction addTransaction = transaction.add(R.id.frame_layout_home, HomePage.newInstance(), TAG_HOME_PAGE);

        addTransaction.commit();
    }


}