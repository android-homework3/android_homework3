package com.example.shopapp.models;

public class Products {

    private int productId;
    private int price;
    private String description;
    private String url;
    private int categoryId;

    public Products(int id, int price, String description, String url, int categoryId) {
        this.productId = id;
        this.price = price;
        this.description = description;
        this.url = url;
        this.categoryId = categoryId;
    }

    public int getId() {
        return productId;
    }

    public void setId(int id) {
        this.productId = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
